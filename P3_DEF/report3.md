# Práctica 3

Daniel Llovio del Rio

20/12/2018

## Prefacio



## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión


## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.

- Ejecución sobre Máquina Virtual VMWare Workstation 14 Player
  - Ubuntu Server 64 bits 16.4
  - Con entorno gráfico
  - 1GB RAM
  - 4 Cores
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU intel i7-6500U
  - CPU 4 cores
  - CPU @2500 MHz (fijada con cpufreq usermode)
  - 8GB RAM
  - 50 procesos en promedio antes de ejecuciones
  - Con entorno gráfico durante ejecuciones

## 2. Diseño e Implementación del Software

La paralelización del código tiene lugar a lo largo de todo el main, comenzando la región paralela tras la creación de las variables.
El primer bucle for, encargado de reservar memoria, se parareliza mediante una directiva for, para repartir sus iteraciones entre los hilos de la región paralela.

Ahora tiene lugar un bucle do-while, en primer lugar, para optimizar el bucle, con la directiva 'single' nos aseguramos que un único thread haga la lectura. A continuación, se crean las tareas mediante la directiva 'task' siempre y cuando se siga leyendo. Para que estas tareas sean unicamente creadas por el thread master, se añade la directiva 'master' antes de la creación de las mismas.

Como el código original produciría fallos a la hora de escribir en el fichero de forma paralela, se incluye un 'taskwait', es decir, una barrera impícita. De esta forma nos aseguramos que todos los hilos completen las tareas antes de comenzar a escribir.

Por último, otra directiva 'for' para paralelizar el bucle que libera la memoria.


## 4. Discusión

He tenido mas dificultades con esta práctica que con las demás prácticas que hacen uso de OpenMP. Tuve errores de paralelismo ya que los hilos no accedian al fichero para escribir correctamente, y me costó solventar este problema. El fichero movie.out se generaba correctamente en todas las ejecuciones pero la maquina virtual me producia algún fallo de memoria.

## Ejercicio 1

#1)

He tenido que modificar el do-while proporcionado por la practica ya que si nos limitamos a paralelizar el código añadiendo las correspondientes directivas de OpenMP, se producía un fallo tras la creación de tareas cuando los threads pasaban a escribir. Esto lo he solucionado añadiendo un bucle además de una barrera.

#2)

-Directiva single: Mediante esta directiva, nos aseguramos que el primer thread que llegue al bloque de código que abarca sea el único que ejecuta dicho código.

-Directiva master: Similar a la directiva single anteriormente descrita, especifica que la región de código será ejecutada únicamente por el thread master, los demas threads saltaran ese bloque de código y proseguirán con su ejecución. En la práctica la utilizamos para asegurarnos de que solo el thread master crea las tareas.

-Directivas task y takwait: Con task creamos bloques de código que se convierten en tareas, estas tareas se añaden a una 'cola' tras crearlas y pasan a ser ejecutadas cuando un thread queda libre. A su vez, taskwait actua como una barrera, se espera hasta que todas las tareas generadas hayan sido completadas.

#3)

-Varibales privadas(i): Es una variable usada para la iteración de los bucles. Esta variable requiere ser privada para que cada hilo tenga una copia de local de ella, de esta forma al modificarse no interferirá con los demás hilos.

-Variables compartidas(pixels,filtered,size,seq): Son variables a las que necesitan tener acceso todos los threads para poder operar sobre ellas y que pueden modificar sin riesgo.

#4)

Al entrar en la región paralela, se crea un conjunto de threads (determinado por la clausula num_threads o por el número de cores de forma predeterminada), habrá varias copias del código que se irán ejecutando por los threads. Al finalizar la región, todos los threads esperan al final de la barrera y finalmente el thread master continua ejecutando el resto del código.
El paralelismo de tareas tiene lugar en el bucle do-while, donde el thread master se encarga de crear las distintas tareas y esperará en la barrera a que todas las tareas programadas hayan sido terminadas por los threads. Estas tareas se encolarán y se irán ejecutando según los threads queden libres.
En los bucles for, las iteraciones del mismo se reparten entre el conjunto de threads mencionados previamente, existiendo también una barrera al finalizar el mismo.

#6)

El fgauss está programado secuencialmente, añadiendo directivas de OpenMP para paralelizarlo podemos obtener una versión paralela mas eficiente que la original.
