
#include <thread>
#include <stdio.h>
#include <string.h>
#include <mutex>
#include <condition_variable>

int size_of_array(double*);
void sum_function(double*,int,int,int);
void xor_function(double*,int,int,int);
void sum_or_xor(int,int, double*);
std::mutex mut;
std::condition_variable cond_var;
bool ready=false;
enum operation{
  sum_enum,
  xor_enum
};

double  results_of_threads=0; //resultados de todos los hilos para el array

int main(int argc, char *argv[]){
  int  array_size;
  int  num_threads;
  char* mode_value;
  enum operation op_mode;
  if(argc>4){
    printf("Faltan parametros");
    exit(-1);
  }else{
    array_size=atoi(argv[1]);
    num_threads=atoi(argv[2]);
    mode_value=argv[3]; //comparar esto en vez de los values de abajo
  }
  double *my_array;
  my_array=(double*) malloc(sizeof(double)*array_size); //creacion dinamica
  for(int i=0;i<array_size;i++){
    my_array[i]=(double)i;
  }
  //Mira que operacion se va a realizar
  if(strcmp(mode_value,"xor")==0){
    op_mode=xor_enum;
  }
  else if(strcmp(mode_value,"sum")==0){
    op_mode=sum_enum;
  }else{
    printf("El modo seleccionado no ha sido xor ni sum");
    exit(-1);
  }
  //Comprueba que modo de funcionamiento
  switch (num_threads) {

    case 1:
    if(op_mode==xor_enum){
      xor_function(my_array,0,num_threads-1,0);
    }else{
      sum_function(my_array,0,num_threads-1,0);
    }
    break;

    default:
     sum_or_xor(op_mode,num_threads,my_array);
  }
}

  int size_of_array(double *my_array){
    return sizeof(my_array)/sizeof(my_array[0]);
  }
  //Calcula la suma en la variable global y actualiza el campo del logger
  void sum_function(double *my_array,int initial,int final,int i){
    double total=0; //calculo de una unica suma
    for(initial;initial<final;initial++){
      total=total+my_array[initial];
      results_of_threads=results_of_threads+my_array[initial];
    }
    printf("La suma total del array ha sido: %d",total);
  }
  //Calcula la xor en la variables global y actualiza el campo del logger
  void xor_function(double *my_array,int initial, int final, int i){

  }

  void sum_or_xor(int op_mode,int num_threads, double *my_array){
    int lenght_parts=size_of_array(my_array)/num_threads;
    int rest=size_of_array(my_array)%num_threads; //resto de la division
    std::thread threads[num_threads];
    int i=0;
    int j=0;
    int h=0;
    double *sub_array;
    for (i=0; i<num_threads; ++i){
      if(i==0 && rest!=0){
        j=j+lenght_parts+rest;
      }else{
        j=j+lenght_parts;
      }
      if(op_mode==xor_enum){
        threads[i] = std::thread(xor_function,sub_array,h,j,i);
        h=h+j;
      }else{
        threads[i] = std::thread(sum_function,sub_array,h,j,i);
        h=h+j;
      }
    }

    for (i=0; i<num_threads; ++i){
      threads[i].join();
      printf("El thread: %d termina",i);
    }
  }
