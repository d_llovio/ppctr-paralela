# Práctica 4

Daniel Llovio del Rio

23/12/2018

## Prefacio

La practica 4 me ha servido para volver a recordar el concepto de sección crítica y repasar el paralelismo con OpenMP.

He conseguido las paralelizaciones propuestas en los distintos ejercicios del guión así como entender las distintos implementaciones de la sección crítica.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión


## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.

- Ejecución sobre Máquina Virtual VMWare Workstation 14 Player
  - Ubuntu Server 64 bits 16.4
  - Con entorno gráfico
  - 1GB RAM
  - 4 Cores
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU intel i7-6500U
  - CPU 4 cores
  - CPU @2500 MHz (fijada con cpufreq usermode)
  - 8GB RAM
  - 50 procesos en promedio antes de ejecuciones
  - Con entorno gráfico durante ejecuciones

## 2. Diseño e Implementación del Software

La paralelización del código tiene lugar en el propio main del programa a traves del uso de directivas de OpenMp. En primer lugar, la sección paralela comienza tras inicializar las variables que se van a usar en la misma.

A lo largo de esta sección, los bloques de código con accesos a memoria o escrituras por pantalla irán precedidos por la directiva 'single' para evitar cualquier tipo de error.
Para la paralelización de los distintos bucles se han incluido directivas 'for' al comienzo de los mismos.

Por último, se ha añadido una directiva 'critical' para proteger el acceso a la varibale c_max, que se trata de una variable compartida.

## 4. Discusión

Con esta práctica he comprendido mejor el uso de las directivas de OpenMP así como la necesidad de controlar el acceso a determinadas variables compartidas, algo fundamental en programación paralela para que los threads funcionen correctamente sin modificar variables que están siendo usadas por otros.

## Ejercicio 1

#1)

-Directiva critical: Esta directiva determina que al comienzo del bloque de código, los threads se quedan esperando hasta que no haya ningún thread ejecutando esa sección.

#2)

-Varibales privadas(i,j,x,y,c): En su mayoría son variables usadas para la iteración de los bucles, esta serie de variables requieren ser privadas para que cada hilo tenga una copia de local de ellas, de esta forma al modificarlas no interferirán con los demás hilos.

-Variables compartidas(count,x_max,x_min,y_max,y_min,count_max,n,c_max,r,g,b): Son variables a las que necesitan tener acceso todos los threads y que pueden modificar sin riesgo.


#3)

Al entrar en la región paralela, se crea un conjunto de threads (determinado por la clausula num_threads o por el número de cores de forma predeterminada), habrá varias copias del código que se irán ejecutando por los threads. Al finalizar la región, todos los threads esperan al final de la barrera y finalmente el thread master continua ejecutando el resto del código.

En los bucles, las iteraciones se reparten entre el conjunto de threads mencionados previamente, existiendo también una barrera al finalizar el mismo.

Hay determinados bloques de código con la directiva single, para que un único thread los ejecute de forma secuencial, estos bloques suelen ser accesos a memoria o escrituras por pantalla.

## Ejercicio 2 Opción A

#Implementación mediante OpenMP: Uso de la directiva crítical

...
#pragma omp for
for (j = 0; j < n; j++) {
  for (i = 0; i < n; i++) {
    #pragma omp critical //seccion critica
    {
    if (c_max < count[i + j * n]) {
      c_max = count[i + j * n];
    }
    }
  }
}
...

#Implementación mediante funciones del runtime

#include <pthread.h>
...
pthread_mutex_t crit=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_lock(&crit);
for (j = 0; j < n; j++) {
  for (i = 0; i < n; i++) {
    if (c_max < count[i + j * n]) {
      c_max = count[i + j * n];
    }
  }
}
pthread_mutex_unlock(&crit);
...

#Implementación secuencial

for (j = 0; j < n; j++) {
  for (i = 0; i < n; i++) {
    {
    if (c_max < count[i + j * n]) {
      c_max = count[i + j * n];
    }
    }
  }
}
