# Práctica 2

Daniel Llovio del Rio

20/12/2018

## Prefacio

La practica 2 ha resultado ser una practica entretenida y un buen primer acercamiento al uso del paradigma de programación OpenMP para la paralelización de las distintas funciones del código.

He conseguido las paralelizaciones propuestas en los distintos ejercicios del guión así como entender los distintos algoritmos de equilibrio de carga
de OpenMP.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión


## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.

- Ejecución sobre Máquina Virtual VMWare Workstation 14 Player
  - Ubuntu Server 64 bits 16.4
  - Con entorno gráfico
  - 1GB RAM
  - 4 Cores
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU intel i7-6500U
  - CPU 4 cores
  - CPU @2500 MHz (fijada con cpufreq usermode)
  - 8GB RAM
  - 50 procesos en promedio antes de ejecuciones
  - Con entorno gráfico durante ejecuciones

## 2. Diseño e Implementación del Software

La paralelización del código tiene lugar tanto en las funciones matmul como matmul_sup mediante el uso de las directivas de OpenMP 'parallel' y 'for'.
En ambos casos, la región paralela abarca desde el final de la inicialización de las variables hasta el acabar el segundo bucle, donde ya se ha finalizado la multiplicación. Antes del comienzo de cada bucle se ha insertado una directiva 'for' para la paralelización de los mismos, tanto en el primero que se encarga de inicializar la matriz 'C' donde se vera el resultado de la multiplicación, como en el segundo, que realiza el algoritmo de multiplicación.

Por último, para conocer el tiempo de ejecución, se crea la variable 'start_exec_time' que contendrá el tiempo de ejecución del programa completo hasta ese punto, dado por la función 'omp_get_wtime' de OpenMP, y una segunda variable llamada 'end_exec_time' con el mismo fin situada al final de la región paralela. Operando con estas dos variables se obtiene el tiempo de ejecución de la función paralela o secuencial.
Adicionalmente, se ha desarrollado un método 'print_matrix' que recorre la matriz pasada como parámetro y la pinta por pantalla.

Para comprobar las funcionalidades del código desarrollado se ha creado un main que inicializa las matrices segun la multiplicación que se vaya a realizar gracias a los metodos 'init_mat_inf' e 'init_mat_sup' y las opera con el algoritmo correspondiente. Adicionalmente imprime los resultados de las distintas multiplicaciones en ficheros para poder comparar los resultados obtenidos así como pintar por pantalla el tiempo de ejecución de las distintas versiones.

## 3. Metodología y desarrollo de las pruebas realizadas

Todas las ejecuciones se han realizado sobre matrices de 960x960 para llegar al tiempo estimado de 10 segundos en la versión secuencial de matmul.

## 4. Discusión

Esta práctica me ha ayudado a tener una base de conocimiento en vista a las siguientes prácticas donde se requerirá una mejor comprension del uso de estas funciones y muchas otras. No he encontrado dificultades  notables y el único problema que tuve fue el pintar en los diferentes ficheros que resolví enseguida gracias a la documentación de C que tenía de cursos pasados.

## Ejercicio 1

#1)

-Directiva parallel: Esta directiva se usa para determinar que el siguiente bloque de código se ejecutará de forma paralela, es decir, se creará un grupo de threads que ejecutarán el codigo de manera paralela. La clausula if sirve para determinar que condición se debe cumplir para que se ejecute el paralelismo. Esto es usado para probar las diferentes versiones del código en una misma ejecución en el main. La clausula private determina que variables serán privadas para cada thread, en este caso serán los distintos índices de los bucles for. Por último la clausula shared determina que varibales serán compartidas entre los threads, esto permite que si la variables es modificada por uno de ellos, todos los demas vean la modificacíon que se ha hecho. En este caso serán las diferentes matrices, para que los distintos threads puedan operar sobre ellas.

-Directiva for: Esta directiva se usa para determinar que las distintas iteraciones del siguiente bucle for se realizarán en paralelo por un conjunto de threads. Esta directiva siempre se incluye dentro de la región paralela para que pueda ejecutarse correctamente.

-Función omp_get_wtime: Devuelve el tiempo de ejecución en segundos del programa hasta dicho punto.

#2)

-Varibales privadas(i,j,k): Son variables usadas para la iteración de los bucles, esta serie de variables requieren ser privadas para que cada hilo tenga una copia de local de ellas, de esta forma al modificarlas no interferirán con los demás hilos.

-Variables compartidas(A,B,C): Son variables a las que necesitan tener acceso todos los threads y que pueden modificar sin riesgo.

#3)

Al entrar en la región paralela, se crea un conjunto de threads (determinado por la clausula num_threads o por el número de cores de forma predeterminada), habrá varias copias del código que se irán ejecutando por los threads. Al finalizar la región, todos los threads esperan al final de la barrera y finalmente el thread master continua ejecutando el resto del código.
En un bucle, las iteraciones del mismo se reparten entre el conjunto de threads mencionados previamente, existiendo también una barrera al finalizar el mismo.

#4)

Para una matriz de 960x960:

Tiempo de ejecución de la versión secuencial: 10.416920 segundos
Tiempo de ejecución de la versión paralela: 5.975584 segundos

Speedup(ganancia)= 10.416920/5.975584 = 1.743247


## Ejercicio 2

#1)

La ganancia ha aumentado ligeramente frente a la versión anterior, este se debe a que la clausula schedule y sus distintos tipos reparten el trabajo entre los threads de forma mas eficiente que si omitieramos esta clausula.

#2)

La versión que ofrece mejores resultados ha sido la de tipo static. Esto se debe a que el tipo static divide las iteraciones entre los distintos threads de forma que cada thread tenga el trabajo de tamaño máximo que se le puede asignar. Esta asigación se da de forma estatica.

#3)

El tamaño de bloque óptimo seria el tamaño máximo, es decir iteraciones/número de threads

#5)

El algortitmo guided reparte el trabajo disminuyendo la carga de este con cada thread. Es decir, hasta alcanzar un tamaño mínimo de 1, el trabajo se reparte de la siguiente manera: la primera iteración se da de tamaño máximo (iteraciones/número de threads), y todas las siguientes tienen un tamaño de : iteraciones restantes/número de threads. Si especificamos un tamaño, este tamaño será el tamaño mínimo a alcanzar por el algorirmo.


##Ejercicio 3

#1)

Al ejecutarse de forma secuencial, tendrá un mayor tiempo de ejecucion que cualquier implementación paralela de la función anterior, matmul_sup.

#2)

El mejor algoritmo creo que sería el dynamic, para que cada pedazo del trabajo sea realizado por el primer thread en acceder a él.

#3)

Al tratarse de matrices triangulares, la carga de trabajo no será la misma al existir muchos elementos cuyo valor será 0, por eso creo que el algoritmo guided ofrecería la peor optimización para la función ya que el trabajo no iria decreciendo proporcionalmente.
