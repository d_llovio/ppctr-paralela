#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <omp.h>

#define RAND rand() % 100

void init_mat_sup (int,float*);
void init_mat_inf (int,float*);
void matmul (float*,float*,float*,int,int);
void matmul_sup (float*,float*,float*,int,int);
void matmul_inf (float*,float*,float*,int);
void print_matrix (float*, int, FILE*);

/* Usage: ./matmul <dim> [block_size]*/

int main (int argc, char* argv[])
{
	int block_size = 1, dim;
	float *A, *B, *C;
	dim = atoi (argv[1]);
	if (argc == 3) block_size = atoi (argv[2]);
	A=(float*) malloc(sizeof(float)*dim*dim);
	B=(float*) malloc(sizeof(float)*dim*dim);
	C=(float*) malloc(sizeof(float)*dim*dim);
	FILE * file_matmul;
	FILE * file_matmul_para;
	FILE * file_matmul_sup;
	FILE * file_matmul_sup_para;
	file_matmul=fopen ("file_matmul.txt", "w+");
	file_matmul_para=fopen ("file_matmul_para.txt", "w+");
	file_matmul_sup=fopen ("file_matmul_sup.txt", "w+");
	file_matmul_sup_para=fopen ("file_matmul_sup_para.txt", "w+");
	init_mat_sup(dim,A);
	init_mat_sup(dim,B);
	matmul(A,B,C,dim,0);
	print_matrix(C,dim,file_matmul);

	matmul(A,B,C,dim,1);
	print_matrix(C,dim,file_matmul_para);

  init_mat_inf(dim,B);
	matmul_sup(A,B,C,dim,0);
	print_matrix(C,dim,file_matmul_sup);

	matmul_sup(A,B,C,dim,1);
	print_matrix(C,dim,file_matmul_sup_para);
	exit (0);
}

void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i)
			M[i*dim+j] = 0.0;
			else
			M[i*dim+j] = RAND;
		}
	}
}

void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
			M[i*dim+j] = 0.0;
			else
			M[i*dim+j] = RAND;
		}
	}
}

void matmul (float *A, float *B, float *C, int dim, int if_para)
{
	double	start_exec_time=omp_get_wtime();
	int i, j, k;
	#pragma omp parallel if(if_para==1) private(i,j,k) shared(A,B,C)
	{

		#pragma omp for
		for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
		C[i*dim+j] = 0.0;

		#pragma omp for //las interaciones del siguiente for se hacen de forma pararela
		for (i=0; i < dim; i++){
			for (j=0; j < dim; j++)
			for (k=0; k < dim; k++)
			C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
		}
	}
	double end_exec_time=omp_get_wtime();
	double	exec_time=end_exec_time-start_exec_time;
	if(if_para==1){
		printf("El tiempo de ejecucion paralelo de matmul ha sido: %f \n",exec_time);
	}else{
		printf("El tiempo de ejecucion secuencial de matmul ha sido: %f \n",exec_time);
	}
}

void matmul_sup (float *A, float *B, float *C, int dim, int if_para)
{
	double	start_exec_time=omp_get_wtime();
	int i, j, k;
	#pragma omp parallel if(if_para==1) private(i,j,k) shared(A,B,C)
	{
		#pragma omp for schedule(guided)
		for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
		C[i*dim+j] = 0.0;

		#pragma omp for schedule(guided)
		for (i=0; i < (dim-1); i++){
			for (j=0; j < (dim-1); j++)
			for (k=(i+1); k < dim; k++)
			C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
		}
	}
	double end_exec_time=omp_get_wtime();
	double	exec_time=end_exec_time-start_exec_time;
	if(if_para==1){
		printf("El tiempo de ejecucion paralelo de matmul_sup ha sido: %f \n",exec_time);
	}else{
		printf("El tiempo de ejecucion secuencial de matmul_sup ha sido: %f \n",exec_time);
	}
}

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
	for (j=1; j < dim; j++)
	for (k=0; k < i; k++)
	C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}
void print_matrix (float *M, int dim, FILE *fp){
	int i, j;
	for (i=0; i < dim; i++){
		for (j=0; j < dim; j++){
			fprintf(fp,"[%f]",M[i*dim+j]);
		}
		fprintf(fp,"\n");
	}
}
